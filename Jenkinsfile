pipeline {
    agent any

    options {
        skipDefaultCheckout(true)
        // Keep the 5 most recent builds
        buildDiscarder(logRotator(numToKeepStr: '5'))
        timestamps()
    }
    environment {
      CONDA_HOME="/var/jenkins_home/miniconda3"
      PATH="$CONDA_HOME/bin:$PATH"
    }

    stages {
        stage ("Pull Code"){
            steps{
                checkout scm
            }
        }
        stage('Prepare Test Environment') {
            steps {
                sh '''conda create --yes -n ${BUILD_TAG} python=3.6
                      source activate ${BUILD_TAG}
                      pip install -r requirements.txt
                      python setup.py install
                   '''
            }
        }
        stage('Unit Tests') {
            steps {
                sh  ''' source activate ${BUILD_TAG}
                        pytest ddl/tests/
                    '''
            }
            post {
                always {
                    // Archive unit tests for the future
                    junit allowEmptyResults: true, testResults: 'test-reports/results.xml'
                }
            }
        }
        stage('Static code metrics') {
            steps {
                echo "Style check"
                sh  ''' source activate ${BUILD_TAG}
                        pylint ddl || true
                    '''
            }
        }
        stage('Generate Sample DDLs') {
            steps {
                sh '''source activate ${BUILD_TAG}
                      generate-ddl ddl/tests/resources/valid_deployment
                   '''
            }
        }
        stage('Build Package') {
            steps {
                sh '''conda build recipe
                   '''
            }
        }
    }
    post {
        always {
            sh 'conda remove --yes -n ${BUILD_TAG} --all'
        }
        failure {
            sh 'conda build purge'
        }
    }
}
