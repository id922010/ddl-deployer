import yaml
import os
import glob


OUTPUT_DIR = 'ddl/output'
ENV_VARIABLE = '%%env%%'
USER_VARIABLE = '%%user%%'


class Deployer(object):
    """
        An object that creates valid Hive DDL statements from a deployment specification file and a
        collection of Hive DDL templates containing environment (ENV_VARIABLE) and user (USER_VARIABLE)
        variables.

        The object is initialized with a folder path that contains both a deployment.yml file and a
        collection of Hive DDL files containing environment and user variables.

        It merges both the deployment.yml and Hive DDL files to generate valid DDLs to be deployed on the clusters
        mentioned on the deployment file.
    """
    def __init__(self, ddl_folder: str):
        """
            :parameter
            - ddl_folder: the folder holding the Hive DDL templates and the deployment.yml file
        """
        self.ddl_folder = ddl_folder
        self.deployments = list()
        self._generated_ddls = dict()
        self._load_deployments()

    def _load_deployments(self):
        """
            Loads the deployment.yml file and creates a list of Deployment objects for each items specified in
            the yml document.
        """
        try:
            with open(self.ddl_folder + '/deployment.yml', 'r') as f:
                try:
                    deployments = yaml.load(f)
                    for deployment in deployments:
                        self.deployments.append(Deployment(deployment['cluster_name'],
                                                           deployment['alias'],
                                                           deployment['database_prefix'],
                                                           deployment['user'],
                                                           deployment['databases']))
                        self._generated_ddls[deployment['cluster_name']] = list()

                except yaml.YAMLError:
                    raise yaml.YAMLError('Not a valid deployment.yml file')
                except TypeError:
                    raise yaml.YAMLError('Not a valid deployment.yml file')
                except KeyError:
                    raise yaml.YAMLError('Not a valid deployment.yml file')

        except FileNotFoundError:
            raise FileNotFoundError('deployment.yml not found')

    def _write_ddl_file(self, ddl_file, deployment):
        # Creates one file per cluster specified in the deployment.yml and appends each Hive DDLs also
        # specified in the deployment.yml
        with open('{0}/{1}'.format(OUTPUT_DIR, deployment.cluster), 'a') as output_ddl:
            with open('{0}/{1}'.format(self.ddl_folder, ddl_file), 'r') as input_ddl:
                output_ddl.write('# Deploying {2}_{0} on {1}\n\n'
                                 .format(ddl_file, deployment.cluster, deployment.db_prefix))
                output_ddl.write(input_ddl.read()
                                 .replace('%%env%%', deployment.db_prefix)
                                 .replace('%%user%%', deployment.db_user))
                output_ddl.write('\n' + '=' * 60 + '\n' * 2)

    def generate_ddl(self, clusters='all', databases='all'):
        """
            Generates DDLs from the deployment file and the DDLs scripts. It creates one file per cluster
            in the OUTPUT_DIR folder.

            :param clusters: list of clusters for which DDLs must be generated. Default to all.
            :param databases: list of databases for which DDLs must be generated. Default to all
            :return: a dictionary of databases generated per cluster.
        """
        # Empty the OUTPUT_DIR
        files = glob.glob(OUTPUT_DIR + '/*')
        for f in files:
            os.remove(f)

        # Generate DDL files
        for ddl_file in os.listdir(self.ddl_folder):
            # Check if the ddl_file must be generated or skip to next DDL file
            if databases != 'all' and ddl_file.split('.')[0] not in databases:
                continue
            for deployment in self.deployments:
                # Check if DDL for the cluster must be generated or skip to next cluster
                if clusters != 'all' and deployment.cluster not in clusters:
                    continue

                if ddl_file.split('.')[0] in deployment.databases:
                    # print('Deploying {2}_{0} on {1}'.format(ddl_file, deployment.cluster, deployment.db_prefix))
                    self._generated_ddls[deployment.cluster].append('{0}_{1}'.format(deployment.db_prefix, ddl_file))
                    self._write_ddl_file(ddl_file, deployment)

        return self._generated_ddls


class Deployment(object):
    """
        A DDL Deployment is a specification of Hive database to be deployed on a Hadoop cluster.
    """
    def __init__(self, cluster: str, alias: str, db_prefix: str, db_user: str, databases: list):
        self.cluster = cluster
        self.alias = alias
        self.db_prefix = db_prefix
        self.db_user = db_user
        self.databases = databases

    def __repr__(self):
        return 'Cluster: {0}, Databases: {1}'.format(self.cluster, self.databases)
