import unittest
import yaml
from ddl import Deployer


class TestDeployer(unittest.TestCase):

    def test_no_deployment_yml(self):
        with self.assertRaises(FileNotFoundError):
            Deployer('dummy folder')

    def test_not_a_yml_file(self):
        with self.assertRaises(yaml.YAMLError):
            Deployer('ddl/tests/resources/not_a_yml_file')

    def test_bad_deployment_yml(self):
        with self.assertRaises(yaml.YAMLError):
            Deployer('ddl/tests/resources/bad_deployment_yml')

    def test_oneDB_oneCluster(self):
        dep = Deployer('ddl/tests/resources/valid_deployment')
        dbs = dep.generate_ddl(clusters='dev-cluster', databases='database1_stg')
        self.assertCountEqual(dbs['dev-cluster'], ['d0_database1_stg.sql',
                                                   't0_database1_stg.sql',
                                                   'a7_database1_stg.sql',
                                                   'p0_database1_stg.sql'])

    def test_oneDB_twoClusters(self):
        dep = Deployer('ddl/tests/resources/valid_deployment')
        dbs = dep.generate_ddl(clusters=['dev-cluster', 'prd-cluster'], databases='database1_stg')
        self.assertCountEqual(dbs['dev-cluster'], ['d0_database1_stg.sql',
                                                   't0_database1_stg.sql',
                                                   'a7_database1_stg.sql',
                                                   'p0_database1_stg.sql'])
        self.assertCountEqual(dbs['prd-cluster'], ['p0_database1_stg.sql'])

    def test_twoDB_oneCluster(self):
        dep = Deployer('ddl/tests/resources/valid_deployment')
        dbs = dep.generate_ddl(clusters='prd-cluster', databases=['database1_stg', 'database3_euds'])
        self.assertCountEqual(dbs['prd-cluster'], ['p0_database1_stg.sql',
                                                   'p0_database3_euds.sql'])

    def test_twoDB_twoClusters(self):
        dep = Deployer('ddl/tests/resources/valid_deployment')
        dbs = dep.generate_ddl(clusters=['dev-cluster', 'prd-cluster'], databases=['database2_ato', 'database3_euds'])
        self.assertCountEqual(dbs['dev-cluster'], ['d0_database2_ato.sql',
                                                   't0_database2_ato.sql',
                                                   'a7_database2_ato.sql',
                                                   'p0_database2_ato.sql',
                                                   'd0_database3_euds.sql',
                                                   't0_database3_euds.sql',
                                                   'a7_database3_euds.sql',
                                                   'p0_database3_euds.sql'])
        self.assertCountEqual(dbs['prd-cluster'], ['p0_database2_ato.sql',
                                                   'p0_database3_euds.sql'])
