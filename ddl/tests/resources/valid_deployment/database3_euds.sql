SET hivevar:EDW_DB=%%env%%_database3_euds;
SET hivevar:EDW_USER=%%user%%;

CREATE DATABASE IF NOT EXISTS ${hivevar:EDW_DB};

ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('comment' = 'Unittest database3');
ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('security' = 'normal');
ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('db_type' = 'euds');
ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('db_object_type' = 'views');
ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('wholesale' = 'yes');
ALTER DATABASE ${hivevar:EDW_DB} SET OWNER USER ${hivevar:EDW_USER};
