SET hivevar:EDW_DB=%%env%%_database2_ato;
SET hivevar:EDW_USER=%%user%%;

CREATE DATABASE IF NOT EXISTS ${hivevar:EDW_DB};

ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('comment' = 'Unittest database2');
ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('security' = 'normal');
ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('db_type' = 'edw');
ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('db_object_type' = 'views');
ALTER DATABASE ${hivevar:EDW_DB} SET DBPROPERTIES ('wholesale' = 'yes');
ALTER DATABASE ${hivevar:EDW_DB} SET OWNER USER ${hivevar:EDW_USER};
