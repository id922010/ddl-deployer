from setuptools import setup

setup(name='ddl-generator',
      version='1.0.1',
      description='Generates Hive DDL statements based on DDL templates and environment files.',
      author='Jonathan Puvilland',
      author_email='jonathan.puvilland@proximus.com',
      packages=['ddl'],
      scripts=['bin/generate-ddl'],
      install_requires=['PyYAML'],
      tests_require=['pylint', 'pytest'],
      )
